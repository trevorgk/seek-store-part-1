import React, { Component } from 'react';
import './App.css';
import { getPricingStrategy } from '../pricing';
import { customers, products } from '../constants';
import AccountSwitcher from './AccountSwitcher';
import AdClicker from './AdClicker';
import Checkout from './Checkout';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: customers[0],
      order: products.map(product => ({
        ...product,
        count: 0,
      })),
    };
  }

  setAccount = account => {
    this.setState({ account });
  };

  updateOrder = (id, count) => {
    let { order } = this.state;

    order = order.map(item => {
      if (item.id !== id) return item;

      return { ...item, count };
    });

    this.setState({ order });
  };

  render() {
    const { account, order } = this.state;
    const pricingStrategy = getPricingStrategy(account);
    const orderWithSubtotals = pricingStrategy.calculatePrices(order);
    return (
      <div className="App">
        <header className="App__header">
          {/* <h1 className="App-title">Welcome to React</h1> */}
          <AccountSwitcher
            account={account}
            accountOptions={customers}
            setAccount={this.setAccount}
          />
        </header>
        <div className="App__selections">
          {order.map(item => (
            <AdClicker key={item.id} update={this.updateOrder} {...item} />
          ))}
        </div>
        <Checkout items={orderWithSubtotals} />
      </div>
    );
  }
}

export default App;
