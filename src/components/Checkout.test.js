import React from 'react';
import { mount } from 'enzyme';
import Checkout, { formatCurrency } from './Checkout';

it('renders without crashing', () => {
  mount(<Checkout />);
});

describe('when items are empty', () => {
  const wrapper = mount(<Checkout items={[]} />);

  it('will not render the list', () => {
    expect(wrapper.find('.Checkout__list')).toHaveLength(0);
  });

  it('will not render the list', () => {
    expect(wrapper.find('.Checkout__list')).toHaveLength(0);
  });

  it('will render the total', () => {
    expect(wrapper.find('.Checkout__total')).toHaveLength(1);
  });
});

const items = [
  { id: 'classic', name: 'Classic Ad', subtotal: 269.99 * 2, count: 2 },
  { id: 'standout', name: 'Standout Ad', subtotal: 0, count: 0 },
  { id: 'premium', name: 'Premium Ad', subtotal: 394.99, count: 1 },
];

describe(`when items are ${JSON.stringify(items)}`, () => {
  const wrapper = mount(<Checkout items={items} />);

  it(`will render 2 ".Checkout__listItem" elements`, () => {
    // only show items with count greater than zero
    expect(wrapper.find('.Checkout__listItem')).toHaveLength(2);
  });

  const expectedTotal = items[0].subtotal + items[2].subtotal;
  it(`will show a total of ${expectedTotal}`, () => {
    // only show items with count greater than zero
    expect(wrapper.find('.Checkout__total').text()).toEqual(
      `Total: ${formatCurrency(expectedTotal)}`,
    );
  });
});
