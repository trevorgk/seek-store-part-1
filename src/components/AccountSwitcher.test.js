import React from 'react';
import { mount } from 'enzyme';
import AccountSwitcher from './AccountSwitcher';

it('renders without crashing', () => {
  mount(<AccountSwitcher />);
});

it('renders a single ".AccountSwitcher__greetings" element with correct text', () => {
  const account = 'Howard Hughes';
  const wrapper = mount(<AccountSwitcher account={account} />);
  const expected = `Hello, ${account}`;

  expect(wrapper.find('.AccountSwitcher__greetings')).toHaveLength(1);
  expect(wrapper.find('.AccountSwitcher__select')).toHaveLength(0);
  expect(wrapper.find('.AccountSwitcher__greetings').text()).toEqual(expected);
});

describe('when toggleActive is true', () => {
  it('renders ".AccountSwitcher__select" with correct options', () => {
    const accountOptions = ['Nike', 'Adidas', 'Etc'];
    const wrapper = mount(<AccountSwitcher accountOptions={accountOptions} />);

    wrapper.find('.AccountSwitcher__revealSelect').simulate('click');

    expect(wrapper.find('.AccountSwitcher__greetings')).toHaveLength(0);
    expect(wrapper.find('.AccountSwitcher__select')).toHaveLength(1);
    expect(wrapper.find('.AccountSwitcher__select option')).toHaveLength(
      accountOptions.length,
    );
  });

  it('fires expected event handlers when an option is selected', () => {
    const setAccountMock = jest.fn();
    const value = 'Absolut';

    const wrapper = mount(<AccountSwitcher setAccount={setAccountMock} />);
    wrapper.find('.AccountSwitcher__revealSelect').simulate('click');

    expect(setAccountMock.mock.calls).toHaveLength(0);

    wrapper
      .find('.AccountSwitcher__select')
      .simulate('change', { target: { value } });

    expect(setAccountMock.mock.calls).toHaveLength(1);
    expect(setAccountMock.mock.calls[0][0]).toBe(value);
  });

  it('hides selectbox and shows greetings when ".AccountSwitcher__hideSelect" is clicked', () => {
    const wrapper = mount(<AccountSwitcher />);

    wrapper.find('.AccountSwitcher__revealSelect').simulate('click');

    expect(wrapper.find('.AccountSwitcher__greetings')).toHaveLength(0);
    expect(wrapper.find('.AccountSwitcher__select')).toHaveLength(1);

    wrapper.find('.AccountSwitcher__hideSelect').simulate('click');

    expect(wrapper.find('.AccountSwitcher__greetings')).toHaveLength(1);
    expect(wrapper.find('.AccountSwitcher__select')).toHaveLength(0);
  });
});
