import React from 'react';
import PropTypes from 'prop-types';
import { compose, withHandlers } from 'recompose';
import './AdClicker.css';

const AdClicker = ({ id, name, price, count, increment, decrement }) => {
  return (
    <div className="AdClicker">
      <button className="AdClicker__decrement" onClick={decrement}>
        -
      </button>
      <span className="AdClicker__name">
        {name} ({count})
      </span>
      <button className="AdClicker__increment" onClick={increment}>
        +
      </button>
    </div>
  );
};

AdClicker.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string,
  price: PropTypes.number,
  count: PropTypes.number.isRequired,
  update: PropTypes.func.isRequired,
};

const enhance = compose(
  withHandlers({
    add: ({ update, id, count }) => val =>
      update(id, count + val >= 0 ? count + val : 0),
  }),
  withHandlers({
    decrement: ({ add }) => () => add(-1),
    increment: ({ add }) => () => add(1),
  }),
);

export default enhance(AdClicker);
