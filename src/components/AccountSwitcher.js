import React from 'react';
import './AccountSwitcher.css';
import PropTypes from 'prop-types';
import { compose, withState, withHandlers } from 'recompose';

const AccountSwitcher = ({
  account,
  setAccount,
  toggleActive,
  setToggleActive,
  accountOptions,
  onChange,
  revealSelect,
  hideSelect,
}) => (
  <div className="AccountSwitcher">
    {toggleActive ? (
      <span>
        Hello,{' '}
        <select
          onChange={onChange}
          className="AccountSwitcher__select"
          value={account}
        >
          {accountOptions.map(option => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </select>
        &nbsp;
        <a
          className="AccountSwitcher__hideSelect"
          // eslint-disable-next-line
          href="javascript:void(0)"
          onClick={hideSelect}
        >
          x
        </a>
      </span>
    ) : (
      <span className="AccountSwitcher__greetings">
        Hello,{' '}
        <a
          className="AccountSwitcher__revealSelect"
          // eslint-disable-next-line
          href="javascript:void(0)"
          onClick={revealSelect}
        >
          {account}
        </a>
      </span>
    )}
  </div>
);

AccountSwitcher.propTypes = {
  account: PropTypes.string,
  setAccount: PropTypes.func,
  toggleActive: PropTypes.bool,
  setToggleActive: PropTypes.func,
  accountOptions: PropTypes.array,
};

AccountSwitcher.defaultProps = {
  accountOptions: [],
  toggleActive: false,
};

export const enhance = compose(
  withState('toggleActive', 'setToggleActive', false),
  withHandlers({
    revealSelect: ({ setToggleActive }) => () => setToggleActive(true),
    hideSelect: ({ setToggleActive }) => () => setToggleActive(false),
    onChange: ({ setToggleActive, setAccount }) => e => {
      const value = e.target.value;

      setAccount(value);
      setToggleActive(false);
    },
  }),
);

export default enhance(AccountSwitcher);
