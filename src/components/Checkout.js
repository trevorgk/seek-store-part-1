import React from 'react';
import PropTypes from 'prop-types';
import { compose, mapProps, defaultProps } from 'recompose';
import './Checkout.css';

export const formatCurrency = number => `$${number.toFixed(2)}`;

const Checkout = ({ filteredItems, total }) => (
  <div className="Checkout">
    <h5 className="Checkout__header">Checkout</h5>
    {filteredItems.length > 0 && (
      <ul className="Checkout__list">
        {filteredItems.map(({ id, name, subtotal, count, discountApplied }) => (
          <li className="Checkout__listItem" key={id}>
            <span className="Checkout__itemQuantity">{count} x </span>
            <span className="Checkout__itemTitle">{name}</span>
            <span
              className={`Checkout__itemPrice ${
                discountApplied ? 'Checkout__itemPrice--discountApplied' : ''
              }`}
            >
              {formatCurrency(subtotal)}
            </span>
          </li>
        ))}
      </ul>
    )}
    <div className="Checkout__total">Total: {formatCurrency(total)}</div>
  </div>
);

Checkout.propTypes = {
  filteredItems: PropTypes.array,
};

Checkout.defaultProps = {
  filteredItems: [],
};

const enhance = compose(
  defaultProps({
    items: [],
  }),
  mapProps(({ items }) => {
    const filteredItems = items.filter(item => item.count > 0);
    const total = filteredItems
      .map(x => x.subtotal)
      .reduce((prev, curr) => prev + curr, 0);

    return {
      filteredItems,
      total,
    };
  }),
);

export default enhance(Checkout);
