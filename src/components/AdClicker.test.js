import React from 'react';
import { mount } from 'enzyme';
import AdClicker from './AdClicker';

let updateMock;
const id = 'default';

beforeEach(() => {
  updateMock = jest.fn();
});

it('renders without crashing', () => {
  mount(<AdClicker update={updateMock} id={id} count={0} />);
});

it('renders a single ".AdClicker__name" element with correct text', () => {
  const name = 'test';
  const count = 7;
  const wrapper = mount(
    <AdClicker update={updateMock} id={id} count={count} name={name} />,
  );
  const expected = `${name} (${count})`;

  expect(wrapper.find('.AdClicker__name')).toHaveLength(1);
  expect(wrapper.find('.AdClicker__name').text()).toEqual(expected);
});

it('renders a single ".AdClicker__decrement" button', () => {
  const wrapper = mount(<AdClicker update={updateMock} id={id} count={0} />);

  expect(wrapper.find('.AdClicker__decrement')).toHaveLength(1);
});

it('calls update method with expected values when ".AdClicker__decrement" button is clicked', () => {
  const wrapper = mount(<AdClicker update={updateMock} id={id} count={1} />);
  const expectedArgs = [id, 0];

  wrapper.find('.AdClicker__decrement').simulate('click');

  expect(updateMock.mock.calls).toHaveLength(1);
  expect(updateMock.mock.calls[0]).toEqual(expectedArgs);
});

it('Will not decrement past 0', () => {
  const wrapper = mount(<AdClicker update={updateMock} id={id} count={0} />);
  const expectedArgs = [id, 0];

  wrapper.find('.AdClicker__decrement').simulate('click');
  expect(updateMock.mock.calls).toHaveLength(1);
  expect(updateMock.mock.calls[0]).toEqual(expectedArgs);
});

it('renders a single ".AdClicker__increment" button', () => {
  const wrapper = mount(<AdClicker update={updateMock} id={id} count={0} />);

  expect(wrapper.find('.AdClicker__increment')).toHaveLength(1);
});

it('calls update method with expected values when ".AdClicker__increment" button is clicked', () => {
  const wrapper = mount(<AdClicker update={updateMock} id={id} count={0} />);
  const expectedArgs = [id, 1];

  wrapper.find('.AdClicker__increment').simulate('click');
  expect(updateMock.mock.calls).toHaveLength(1);
  expect(updateMock.mock.calls[0]).toEqual(expectedArgs);
});
