import customers from './customers';
import products from './products';

export { customers, products };
