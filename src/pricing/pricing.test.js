import { getPricingStrategy } from './';
import GenericPricingStrategy from './GenericPricingStrategy';
import NikePricingStrategy from './NikePricingStrategy';
import ApplePricingStrategy from './ApplePricingStrategy';
import UnileverPricingStrategy from './UnileverPricingStrategy';
import FordPricingStrategy from './FordPricingStrategy';
import products from '../constants/products';

describe('when customer is "Anonymous"', () => {
  const order = [
    { id: products[0].id, count: 1 },
    { id: products[1].id, count: 1 },
    { id: products[2].id, count: 1 },
  ];

  const strategy = getPricingStrategy('Anonymous');
  it('Should return generic strategy', () => {
    expect(strategy).toBeInstanceOf(GenericPricingStrategy);
  });
  const result = strategy.calculatePrices(order);
  it('should not apply any special prices', () => {
    expect(result[0].subtotal).toEqual(products[0].price);
    expect(result[1].subtotal).toEqual(products[1].price);
    expect(result[2].subtotal).toEqual(products[2].price);
  });
});

describe('when customer is "Nike"', () => {
  const order = [{ id: 'premium', count: 6 }];
  const strategy = getPricingStrategy('Nike');
  it('Should return Nike strategy', () => {
    expect(strategy).toBeInstanceOf(NikePricingStrategy);
  });
  const result = strategy.calculatePrices(order);
  it('should apply correct pricing rule', () => {
    expect(result[0].subtotal).toEqual(379.99 * 6);
  });
});

describe('when customer is "Ford"', () => {
  const order = [
    {
      id: 'classic',
      count: 11,
    },
    {
      id: 'standout',
      count: 1,
    },
    {
      id: 'premium',
      count: 4,
    },
  ];
  const strategy = getPricingStrategy('Ford');
  it('Should return Ford strategy', () => {
    expect(strategy).toBeInstanceOf(FordPricingStrategy);
  });
  const result = strategy.calculatePrices(order);
  it('should apply correct pricing rule', () => {
    expect(result[0].subtotal).toEqual(269.99 * 9);
    expect(result[1].subtotal).toEqual(309.99);
    expect(result[2].subtotal).toEqual(389.99 * 4);
  });
});

describe('when customer is "Unilever"', () => {
  const order = [
    {
      id: 'classic',
      count: 7,
    },
  ];
  const strategy = getPricingStrategy('Unilever');
  it('Should return Unilever strategy', () => {
    expect(strategy).toBeInstanceOf(UnileverPricingStrategy);
  });
  const result = strategy.calculatePrices(order);
  it('should apply correct pricing rules', () => {
    expect(result[0].subtotal).toEqual(269.99 * 5);
  });
});

describe('when customer is "Apple"', () => {
  const order = [{ id: 'standout', count: 4 }];
  const strategy = getPricingStrategy('Apple');
  it('Should return Apple strategy', () => {
    expect(strategy).toBeInstanceOf(ApplePricingStrategy);
  });
  const result = strategy.calculatePrices(order);
  it('should apply correct pricing rules', () => {
    expect(result[0].subtotal).toEqual(299.99 * 4);
  });
});
