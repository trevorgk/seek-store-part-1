import PricingStrategy from './PricingStrategy';

class FordPricingStrategy extends PricingStrategy {
  calculatePrices = order =>
    order.map(item => {
      const price = this.getStandardPrice(item.id);

      if (item.id === 'classic') {
        item.subtotal =
          Math.floor(item.count / 5) * (price * 4) + (item.count % 5) * price;
        item.discountApplied = true;
      } else if (item.id === 'standout') {
        item.subtotal = item.count * 309.99;
        item.discountApplied = true;
      } else if (item.id === 'premium' && item.count >= 3) {
        item.subtotal = 389.99 * item.count;
        item.discountApplied = true;
      } else {
        item.subtotal = price * item.count;
        item.discountApplied = false;
      }

      return item;
    });
}

export default FordPricingStrategy;
