import PricingStrategy from './PricingStrategy';

class ApplePricingStrategy extends PricingStrategy {
  calculatePrices = order =>
    order.map(item => {
      const price = this.getStandardPrice(item.id);
      if (item.id === 'standout') {
        item.subtotal = 299.99 * item.count;
        item.discountApplied = true;
      } else {
        item.subtotal = price * item.count;
        item.discountApplied = false;
      }

      return item;
    });
}

export default ApplePricingStrategy;
