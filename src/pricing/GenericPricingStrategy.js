import PricingStrategy from './PricingStrategy';

class GenericPricingStrategy extends PricingStrategy {
  calculatePrices = order =>
    order.map(item => {
      const price = this.getStandardPrice(item.id);
      item.subtotal = price * item.count;
      item.discountApplied = false;

      return item;
    });
}

export default GenericPricingStrategy;
