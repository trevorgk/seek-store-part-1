import PricingStrategy from './PricingStrategy';

class NikePricingStrategy extends PricingStrategy {
  calculatePrices = order =>
    order.map(item => {
      const price = this.getStandardPrice(item.id);

      if (item.id === 'premium' && item.count >= 4) {
        item.subtotal = 379.99 * item.count;
        item.discountApplied = true;
      } else {
        item.subtotal = price * item.count;
        item.discountApplied = false;
      }

      return item;
    });
}

export default NikePricingStrategy;
