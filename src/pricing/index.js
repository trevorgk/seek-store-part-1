import GenericPricingStrategy from './GenericPricingStrategy';
import NikePricingStrategy from './NikePricingStrategy';
import ApplePricingStrategy from './ApplePricingStrategy';
import UnileverPricingStrategy from './UnileverPricingStrategy';
import FordPricingStrategy from './FordPricingStrategy';

export const getPricingStrategy = account => {
  switch (account) {
    case 'Nike':
      return new NikePricingStrategy();
    case 'Unilever':
      return new UnileverPricingStrategy();
    case 'Apple':
      return new ApplePricingStrategy();
    case 'Ford':
      return new FordPricingStrategy();
    default:
      return new GenericPricingStrategy();
  }
};
