import PricingStrategy from './PricingStrategy';

class UnileverPricingStrategy extends PricingStrategy {
  calculatePrices = order =>
    order.map(item => {
      const price = this.getStandardPrice(item.id);
      if (item.id === 'classic') {
        item.subtotal =
          Math.floor(item.count / 3) * (price * 2) + (item.count % 3) * price;
        item.discountApplied = true;
      } else {
        item.subtotal = price * item.count;
        item.discountApplied = false;
      }

      return item;
    });
}

export default UnileverPricingStrategy;
